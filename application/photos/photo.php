<?php
$page = Page::getInstance("CMS :: Photos", ["main_menu", "login", "photos"]);
$page->getHeader();
include_once(Constants::$applicationPath . "/menu.php");
$thisModule = $page->getModule("photos");

if(!isset($_GET["albumId"]) || !isset($_GET["photoId"])) {
    redirect("photos");
}

$photo = $thisModule->getOnePhoto($_GET["photoId"]);
if(!$photo) {
    echo "Wrong photo id!";
    redirect("photos", 3);
    exit;
}

$webPath = Constants::$webPath . "/public_files/photos/$_GET[albumId]";
$back = Constants::$webPath . "/photos/album?id=$_GET[albumId]";

echo <<<EOF
<div class="content">
    <a href="$back"> < </a>
    <h1>{$photo["photoTitle"]}</h1>
    Description {$photo["photoDescription"]}
    <br/>
    <a href="$webPath/$photo[photoFile]">
        <img style="width:100%" src="$webPath/$photo[photoFile]">
    </a>
</div>
EOF;

?>


<?php
$page->getFooter();
?>