<?php
$page = Page::getInstance("MeehaiXYZ :: Binance :: Balance", ["main_menu", "login"]);
$page->getHeader();
$loginModule = $page->getModule("login");
include_once(Constants::$applicationPath . "/menu.php");

$balancePath = Constants::$webPath . "/balance.png";

echo <<<EOF
<div class="content">
    <h3> Automating money losing since 2021.</h3> <br/>
    <img src="$balancePath" id="mainFig"> 
</div>

<script type="text/javascript">
    function updateImage() {
        document.getElementById("mainFig").src = "$balancePath?" + new Date().getTime();
    }

    updateImage();
    window.setInterval(updateImage, 500);
</script>
EOF;

$page->getFooter();



