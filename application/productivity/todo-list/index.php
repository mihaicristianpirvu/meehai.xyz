

<?php
$page = Page::getInstance("MeehaiXYZ :: ToDo-List", ["login", "TodoList", "main_menu"]);
$todoModule = $page->getModule("TodoList");
$loginModule = $page->getModule("login");
includeJS("https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js");
includeCSS(Constants::$webPath . "/application/productivity/todo-list/style.css");

if(!$loginModule->isLogged()) {
	redirectWithMessage("login", 3, "You need to be logged in to use to do list.");
	exit;
}

if(isset($_POST["newTask"])) {
	/* If we add to a specific day (not today), we can't infer the hour. However, for today, we use the timestamp */
	$date = isset($_GET["day"]) && $_GET["day"] !== date("Ymd") ? $_GET["day"] . "000000" : date("YmdHis");

	$todoModule->addTask($_POST["newTask"], NULL, -1, "WIP", $date);
	header("Refresh:0");
	exit;
}

if(isset($_POST["updateTask"])) {
	assert (isset($_POST["taskStatus"]) && ($_POST["taskStatus"] == "WIP" || $_POST["taskStatus"] == "DONE"));
	$newStatus = $_POST["taskStatus"] == "WIP" ? "DONE" : "WIP";
	$todoModule->updateTaskStatus($_POST["taskId"], $newStatus);
	exit;
}

include_once(Constants::$applicationPath . "/menu.php");

function tasksTableHtml($todayTasks) {
	$tasksStr = <<<EOF
		<table id="tasksTable">
			<tr>
				<th> Title </th>
				<th> Description </th>
				<th> Status </th>
				<th> Update </th>
			<tr/>
	EOF;
	for ($i=0; $i<count($todayTasks); $i++) {
		$task = $todayTasks[$i];
		$tasksStr .= <<<EOF
		<tr>
			<td id=$task[id]> $task[title] </td>
			<td> $task[description] </td>
			<td> $task[status] </td>
			<td> <span> Click! </span> </td>
		</tr>
		EOF;
	}
	$tasksStr .= "</table>";
	return $tasksStr;
}

function calendarStr($counts) {
	$keys = array_keys($counts);
	$str = "<br/>";
	for($r=0;$r<7;$r++) {
		$str .= "<div style=\"margin:1px\">";
		for($c=0;$c<365/7-1;$c++) {
			$key = $keys[$c * 7 + $r + 2];
			$count = $counts[$key]["DONE"];
			$color = max(0, 255 - $count * 30);
			$strColor = "rgb($color, $color, 255)";
			$path = Constants::$webPath . "/productivity/todolist?day=$key";
			$str .= <<<EOF
			<a href="$path" class="calendar" >
				<div style="background-color:$strColor;">
					<div id="hover-content"> Tasks done on $key: $count </div>
				</div>
			</a>
			EOF;
		}
		$str .= "</div>";
	}
	return $str;
}

$strFormat = isset($_GET["day"]) ? $_GET["day"] : date("Ymd");
assert (DateTime::createFromFormat("Ymd", $strFormat) !== FALSE);
$relevantDay = new DateTime($strFormat);
$strRelevant = isset($_GET["day"]) ? $_GET["day"] : "Today";
$dayTasks = $todoModule->getDayTasks($relevantDay);
$tasksStr = tasksTableHtml($dayTasks);

$today = new DateTime();
$oneYearAgo = (new DateTime())->sub(new DateInterval("P1Y"));
$res = $todoModule->getCountEntriesInInterval($oneYearAgo, $today);
$tableStr = calendarStr($res);

echo <<<EOF
<div class="content">
	Hello $_SESSION[user]. A beautiful day TO DO TASKS! <br/><br/>
	<div>
	New task:
	<form method="POST">
		<input type="text" name="newTask">
		<input type="submit" value="Add!">
	</form>
	</div>
	<br/><br/>

	<div>
	$strRelevant tasks: $tasksStr
	</div>
	<br/><br/>

	<div>
	Streak calendar: $tableStr
	</div>
</div>
EOF;

includeJS(Constants::$webPath . "/application/productivity/todo-list/index.js");
?>
<?php

?>
