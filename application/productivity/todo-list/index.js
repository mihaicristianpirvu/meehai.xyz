function updateTaskStatus() {
    $("#tasksTable tr td span").on("click", function() {
        var currentRow = $(this).closest("tr");
        var tds = currentRow.find("td");
        var id = tds[0].getAttribute("id");
        var currentStatus = tds[2].innerText;
        var newStatus = currentStatus == "WIP" ? "DONE" : "WIP";
        var posting = $.post("todolist", {updateTask: true, taskId: id, taskStatus: currentStatus});
        posting.done(function(data) {
            tds[2].innerHTML = newStatus;
        });
    });
}

updateTaskStatus();