if [ $# -ne 1 ]; then
	echo "Usage: ./clean <valmih_CMS_path>";
	exit 1;
fi

vmPath=`realpath $1`;
echo "Clean(s)ing to $vmPath";

if [ ${vmPath: -1} == "/" ]; then
	vmPath=${vmPath:0:-1};
fi

function msgExit {
	echo $1;
	exit 1;
}

function wrongVMPath {
	echo "Wrong ValMih CMS path directory";
	echo "Recommended command: 'git clone https://gitlab.com/valmih-cms/valmih-CMS $1' and follow web server instalation steps from README";
	exit 1;
}

[ -d $vmPath ] || wrongVMPath $vmPath;
[ -d "$vmPath/core" ] || wrongVMPath $vmPath;
[ -d "$vmPath/modules" ] || wrongVMPath $vmPath;
[ -d "$vmPath/themes" ] || wrongVMPath $vmPath;

# Update submodules
pwd=`pwd`

echo "Application"
rm -rf $vmPath/application
echo "Theme"
rm -rf $vmPath/themes/MeehaiXYZ
echo "DB";
rm -rf $vmPath/db
echo "Modules"
rm -rf $vmPath/modules
cd $vmPath;
git reset --hard HEAD;
git pull;
cd $pwd;

